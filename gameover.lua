--[========================================================================[--

End of game screen for Thrust II Reloaded.

Copyright © 2015-2018 Pedro Gimeno Fortea

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

--]========================================================================]--

local gameover = {}

local la,le,lfs,lf,lg,li,lj,lk,lm,lmo,lp,ls,lsys,lth,lt,lw = require'ns'()

local snd
local sndstate = "stopped"

function gameover.load()
  snd = la.newSource("snd/EndGame.ogg", "stream")
end

function gameover.activate()
  gameover.timer = 0
  snd:play()
  sndstate = "playing"
end

function gameover.deactivate()
  snd:stop()
  sndstate = "stopped"
end


function gameover.pause(paused)
  if paused then
    if sndstate == "playing" then
      snd:pause()
      sndstate = "paused"
    end
  elseif sndstate == "paused" then
    snd:play()
    sndstate = "playing"
  end
end

function gameover.keypressed(k, r)
  if r then return end
  if k == "escape" then
    return main.dialog("EXIT TO MENU?", main.tomenu)
  end
end

function gameover.update(dt)
  gameover.timer = gameover.timer + dt
  if gameover.timer > 12 then
    main.activate(screens.splash)
    return
  end
  -- sound is not looped - update sndstate
  if sndstate == "playing" and not snd:isPlaying() then
    sndstate = "stopped"
  end
end

function gameover.draw()
  main.centertext("GAME OVER", main.wcx, main.wcy)
end

return gameover
