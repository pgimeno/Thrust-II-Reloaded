--[========================================================================[--

Abbreviate LÖVE namespaces as locals

Written by Pedro Gimeno Fortea, donated to the public domain.

Usage (copy-paste this line to every file needing the namespaces):

local la,le,lfs,lf,lg,li,lj,lk,lm,lmo,lp,ls,lsys,lth,lt,lw,ld,lto,lv = require'ns'()

--]========================================================================]--

return function() return
  love.audio, love.event, love.filesystem, love.font, love.graphics,
  love.image, love.joystick, love.keyboard, love.math, love.mouse,
  love.physics, love.sound, love.system, love.thread, love.timer, love.window,
  love.data, love.touch, love.video
end
