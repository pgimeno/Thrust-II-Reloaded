--[========================================================================[--

Get Ready screen for Thrust II Reloaded.

Copyright © 2015-2018 Pedro Gimeno Fortea

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

--]========================================================================]--

local getready = {}

local la,le,lfs,lf,lg,li,lj,lk,lm,lmo,lp,ls,lsys,lth,lt,lw = require'ns'()

local timer
local wait_time = 1.2
local scroll_in_time = 1.2
local quad, prev_x, canvas

local DIV = love_version >= 11000000 and 255 or 1

function getready.load()
  quad = lg.newQuad(0, 0, 1, 1, main.ww, main.wh)
  canvas = lg.newCanvas(main.ww, main.wh) -- for scrolling
end

function getready.activate()
  if getready.fromstart then
    timer = -wait_time
  else
    timer = 0
  end
  prev_x = main.ww + 8
end

function getready.keypressed(k, r)
  if r then return end
  if k == "escape" then
    return main.dialog("EXIT TO MENU?", main.tomenu)
  end
  if k == "f3" then
    main.restore = true
    screens.game.newgame()
    getready.fromstart = true
    getready.activate()
  end
end

function getready.update(dt)
  timer = timer + dt
  if timer >= scroll_in_time then
    main.activate(screens.game)
    return
  end
end

function getready.draw()
  local game = screens.game
  lg.setScissor(game.vx, game.vy, game.vw, game.vh)
  local x = (main.ww + 8)

  if timer > 0 then
    x = math.floor(x * (1 - timer/scroll_in_time))
  end

  if getready.fromstart and timer < 0 then
    lg.setColor(0/DIV,0/DIV,0/DIV,255/DIV)
    lg.rectangle("fill", game.vx, game.vy, game.vw, game.vh)
    lg.setColor(255/DIV,255/DIV,255/DIV,255/DIV)
    main.centertext("GET READY", game.vx+game.vw/2, game.vy+game.vh/2)
  end

  if timer >= 0 then
    -- Drawing part of a canvas onto itself causes artifacts,
    -- therefore we draw to our local canvas and copy the result back.
    lg.setCanvas(canvas)
    lg.clear()

    if x >= 0 then
      quad:setViewport((prev_x-x), 0, game.vw-(prev_x-x), game.vh)
      lg.draw(main.canvas, quad, 0, 0)
      prev_x = x
    end
    if x <= game.vw then
      lg.setScissor(x + game.vx, game.vy, game.vw-x, game.vh)

      local vpx = math.floor(game.state.ship.x+0.5-game.vw/2)
      local vpy = math.floor(game.state.ship.y+0.5-game.vh/2)
      if vpy < 0 then vpy = 0 end
      if vpy > 2048-game.vh then vpy = 2048-game.vh end
      game.tiles_draw(vpx-x, vpy)
      game.orbs_draw(vpx-x, vpy)

      lg.setScissor(game.vx, game.vy, game.vw, game.vh)
    end
    lg.setColor(120/DIV,150/DIV,150/DIV,255/DIV)
    lg.rectangle("fill", x-8, 0, 8, main.wh)
    lg.setColor(255/DIV,255/DIV,255/DIV,255/DIV)

    -- Draw back to the main canvas
    lg.setCanvas(main.canvas)
    lg.draw(canvas)
  end
  lg.setScissor()
end

function getready.resize(neww, newh)
  canvas = lg.newCanvas(main.ww, main.wh)
  quad = lg.newQuad(0, 0, 1, 1, main.ww, main.wh)
end

return getready
