--[========================================================================[--

Key redefinition screen code for Thrust II Reloaded.

Copyright © 2015-2018 Pedro Gimeno Fortea

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

--]========================================================================]--

local redef = {}

local la,le,lfs,lf,lg,li,lj,lk,lm,lmo,lp,ls,lsys,lth,lt,lw = require'ns'()

local step

local DIV = love_version >= 11000000 and 255 or 1

function redef.activate()
  step = 1
end

local function xlate(s)
  return s == " " and "SPACE" or s:upper()
end

function redef.draw()
  lg.setColor(192/DIV, 192/DIV, 192/DIV, 255/DIV)
  main.centertext("DEFINE KEYS", main.wcx, 56)
  lg.print("PRESS KEY FOR LEFT.....", 104, 112)
  if step >= 2 then
    lg.print(xlate(main.keys.left), 480, 112)
    lg.print("PRESS KEY FOR RIGHT....", 104, 160)
  end
  if step >= 3 then
    lg.print(xlate(main.keys.right), 480, 160)
    lg.print("PRESS KEY FOR THRUST...", 104, 208)
  end
  if step >= 4 then
    lg.print(xlate(main.keys.thrust), 480, 208)
    lg.print("PRESS KEY FOR PICKUP...", 104, 256)
  end
  if step >= 5 then
    lg.print(xlate(main.keys.pickup), 480, 256)
    lg.print("PRESS KEY FOR FIRE.....", 104, 304)
  end
  if step >= 6 then
    lg.print(xlate(main.keys.fire), 480, 304)
    lg.print("IS THIS CORRECT? (Y/N)", 128, 384)
  end
  lg.setColor(255/DIV, 255/DIV, 255/DIV)
end

function redef.keypressed(k, r)
  -- F10 reserved for save game, F3 for restore
  if k == "f10" or k == "f3" then return end
  -- exit this screen with ESC
  if k == "escape" then
    -- abort
    main.activate(screens.menu)
    return
  end
  -- pause reserved for pausing the game
  if k == "pause" then return end
  if step == 1 then
    main.keys.left = k
  elseif step == 2 then
    main.keys.right = k
  elseif step == 3 then
    main.keys.thrust = k
  elseif step == 4 then
    main.keys.pickup = k
  elseif step == 5 then
    main.keys.fire = k
  end
  if step >= 1 and step <= 5 then
    step = step + 1
  end
end

function redef.textinput(c)
  if step == 6 then
    if c:lower() == "y" then
      -- save and return
      local s = main.keys.left   .. "\n"
             .. main.keys.right  .. "\n"
             .. main.keys.thrust .. "\n"
             .. main.keys.pickup .. "\n"
             .. main.keys.fire
      lfs.write("keys.txt", s, #s)
      main.activate(screens.menu)
      return
    elseif c:lower() == "n" then
      -- start again
      step = 1
    end
  end
end

return redef
