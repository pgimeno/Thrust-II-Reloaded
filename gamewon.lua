--[========================================================================[--

Game Won screen for Thrust II Reloaded.

Copyright © 2015-2018 Pedro Gimeno Fortea

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

--]========================================================================]--

local gamewon = {}

local la,le,lfs,lf,lg,li,lj,lk,lm,lmo,lp,ls,lsys,lth,lt,lw = require'ns'()

local timer
local rays
local gdt
local rayspeed = 1000

local function newray()
  local amplitude = 1
  local rnd = 2*amplitude*lm.random() - amplitude
  local angle = math.abs(rnd)^3
  if rnd < 0 then angle = -angle end
  local spdrnd = lm.random()*0.7+1
  local vx = math.sin(angle)*spdrnd
  local vy = -math.cos(angle)*spdrnd
  return { x = screens.game.vw/2 + screens.game.vx, y = 255,
      vx = vx, vy = vy,
      len = math.random()*65
  }
end

function gamewon.activate()
  timer = 0
  rays = {}
end

function gamewon.deactivate()
  rays = {}
end

function gamewon.update(dt)
  timer = timer + dt
  if timer > 35 then
    main.activate(screens.splash)
    return
  end
  if timer >= 5 and timer < 20 then
    for k,v in ipairs(rays) do
      v.y = v.y + v.vy * rayspeed * dt
      if v.y < 0 then
        if timer < 19.5 then
          rays[k] = newray()
        end
      else
        v.x = v.x + v.vx * rayspeed * dt
      end
    end
    local t = math.floor((timer-5) / 0.05) -- 0.1 is the spawn rate
    if t < 80 then -- max simultaneous rays
      rays[t] = newray()
    end
  end
end

function gamewon.draw()
  local y
  if timer < 2 then
    y = 90
  elseif timer < 3 then
    y = math.floor(90.5 - (timer-2)*90)
  else
    y = 0
  end
  local game = screens.game
  game.tiles_draw(math.floor(1856-game.vw/2), y)
  if timer < 5 then
    -- do nothing
  elseif timer < 20 then
    lg.setScissor(game.vx, game.vy, game.vw, game.vh)
    for k,v in ipairs(rays) do
      lg.line(v.x, v.y, v.x+v.vx*v.len, v.y+v.vy*v.len)
    end
    lg.setScissor()
  end
end

function gamewon.keypressed(k, r)
  if r then return end
  if k == "escape" then
    return main.dialog("EXIT TO MENU?", main.tomenu)
  end
  -- DEBUG
  if k == "f3" then
    gamewon.activate()
  end
end

return gamewon
