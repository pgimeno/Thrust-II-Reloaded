--[========================================================================[--

Map and ancillary data for Thrust II Reloaded.

Copyright © 2015-2018 Pedro Gimeno Fortea

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

--]========================================================================]--

local map = {}

local la,le,lfs,lf,lg,li,lj,lk,lm,lmo,lp,ls,lsys,lth,lt,lw = require'ns'()

do
  local found = false
  local i = 0
  for line in lfs.lines('tilesets/ThrustIIgen4.tmx') do
    if found then
      if line == '</data>' then
        break
      end
      for match in line:gmatch('[^%n,]+') do
        i = i + 1
        map[i] = tonumber(match)
      end
    elseif line:find('<data encoding="csv">', 0, true) then
      found = true
    end
  end
end

local enemytypes = {
  -- they are defined in order of sprites
  { -- type 1
    width = 1, -- number of consecutive tiles to draw
    height = 1, -- first all width tiles are drawn before skipping to the next
                -- height tile
    nframes = 8 ,    -- number of consecutive sprites in sprite image
    pingpong = true, -- animation pingpong
                     -- implies a pause at the endpoints
  },
  { -- type 2
    width = 1,
    height = 1,
    nframes = 8,
    pingpong = false,
  },
  { -- type 3
    width = 1,
    height = 1,
    nframes = 8,
    pingpong = false,
  },
  { -- type 4
    width = 1,
    height = 1,
    nframes = 8,
    pingpong = false,
  },
  { -- type 5
    width = 1,
    height = 1,
    nframes = 8,
    pingpong = true,
  },
  { -- type 6
    width = 1,
    height = 1,
    nframes = 8,
    pingpong = true,
  },
  { -- type 7
    width = 2,
    height = 1,
    nframes = 8,
    pingpong = true,
  },
  { -- type = 8
    width = 2,
    height = 1,
    nframes = 8,
    pingpong = false,
  },
}

local enemies = {
  {
    type = 8,
    x0 = 206, y0 = 834, -- from coordinates
    x1 = 206, y1 = 914, -- to coordinates
    period = 5, -- total time to move (seconds)
    t = 2.25,   -- starting/current instant in time
    frame = 4,  -- starting/current frame
    fps = 16,   -- animation fps
  },
  {
    type = 8,
    x0 = 2766, y0 = 1198,
    x1 = 2766, y1 = 1262,
    period = 4,
    t = 0.125,
    frame = 2,
    fps = 16,
  },
  {
    type = 8,
    x0 = 702, y0 = 1414,
    x1 = 702, y1 = 1494,
    period = 5,
    t = 1.75,
    frame = 4,
    fps = 16,
  },
  {
    type = 8,
    x0 = 3566, y0 = 1718,
    x1 = 3566, y1 = 1782,
    period = 4,
    t = 0.1875,
    frame = 3,
    fps = 16,
  },
  {
    type = 8,
    x0 = 3582, y0 = 836,
    x1 = 3582, y1 = 916,
    period = 5,
    t = 1.875,
    frame = 6,
    fps = 16,
  },
  {
    type = 8,
    x0 = 3086, y0 = 724,
    x1 = 3086, y1 = 788,
    period = 4,
    t = 1.1875,
    frame = 3,
    fps = 16,
  },
  {
    type = 8,
    x0 = 1870, y0 = 1156,
    x1 = 1870, y1 = 1236,
    period = 5,
    t = 0.5625,
    frame = 1,
    fps = 16,
  },
  {
    type = 8,
    x0 = 1694, y0 = 1796,
    x1 = 1694, y1 = 1876,
    period = 5,
    t = 0.3125,
    frame = 5,
    fps = 16,
  },
  {
    type = 3,
    x0 = 2848, y0 = 1804,
    x1 = 2848, y1 = 1852,
    period = 3,
    t = 1.375,
    frame = 6,
    fps = 16,
  },
  {
    type = 3,
    x0 = 1472, y0 = 1162,
    x1 = 1472, y1 = 1242,
    period = 5,
    t = 1.625,
    frame = 2,
    fps = 16,
  },
  {
    type = 3,
    x0 = 2912, y0 = 912,
    x1 = 2912, y1 = 1024,
    period = 7,
    t = 1.5,
    frame = 0,
    fps = 16,
  },
  {
    type = 3,
    x0 = 2448, y0 = 1168,
    x1 = 2448, y1 = 1248,
    period = 5,
    t = 2.1875,
    frame = 3,
    fps = 16,
  },
  {
    type = 3,
    x0 = 1104, y0 = 776,
    x1 = 1104, y1 = 920,
    period = 9,
    t = 1.375,
    frame = 6,
    fps = 16,
  },
  {
    type = 3,
    x0 = 384, y0 = 1216,
    x1 = 384, y1 = 1312,
    period = 6,
    t = 1,
    frame = 0,
    fps = 16,
  },
  {
    type = 3,
    x0 = 1312, y0 = 1548,
    x1 = 1312, y1 = 1628,
    period = 5,
    t = 2.125,
    frame = 2,
    fps = 16,
  },
  {
    type = 3,
    x0 = 176, y0 = 1812,
    x1 = 176, y1 = 1876,
    period = 4,
    t = 1.5,
    frame = 0,
    fps = 16,
  },
  {
    type = 2,
    x0 = 4064, y0 = 844,
    x1 = 4064, y1 = 908,
    period = 4,
    t = 1.875,
    frame = 6,
    fps = 16,
  },
  {
    type = 2,
    x0 = 1376, y0 = 968,
    x1 = 1376, y1 = 1048,
    period = 5,
    t = 2.3125,
    frame = 5,
    fps = 16,
  },
  {
    type = 2,
    x0 = 4000, y0 = 1744,
    x1 = 4000, y1 = 1888,
    period = 9,
    t = 3.3125,
    frame = 5,
    fps = 16,
  },
  {
    type = 2,
    x0 = 928, y0 = 1292,
    x1 = 928, y1 = 1356,
    period = 4,
    t = 1.125,
    frame = 2,
    fps = 16,
  },
  {
    type = 2,
    x0 = 2208, y0 = 590,
    x1 = 2208, y1 = 734,
    period = 9,
    t = 3.3125,
    frame = 5,
    fps = 16,
  },
  {
    type = 2,
    x0 = 2448, y0 = 1548,
    x1 = 2448, y1 = 1708,
    period = 10,
    t = 3.6875,
    frame = 3,
    fps = 16,
  },
  {
    type = 2,
    x0 = 1504, y0 = 1352,
    x1 = 1504, y1 = 1512,
    period = 10,
    t = 0.75,
    frame = 4,
    fps = 16,
  },
  {
    type = 2,
    x0 = 544, y0 = 1802,
    x1 = 544, y1 = 1882,
    period = 5,
    t = 1.5625,
    frame = 1,
    fps = 16,
  },
  {
    type = 4,
    x0 = 912, y0 = 918,
    x1 = 912, y1 = 982,
    period = 4,
    t = 0.6875,
    frame = 3,
    fps = 16,
  },
  {
    type = 4,
    x0 = 2992, y0 = 1552,
    x1 = 2992, y1 = 1600,
    period = 3,
    t = 1.0625,
    frame = 1,
    fps = 16,
  },
  {
    type = 4,
    x0 = 2816, y0 = 930,
    x1 = 2816, y1 = 994,
    period = 4,
    t = 1.3125,
    frame = 5,
    fps = 16,
  },
  {
    type = 4,
    x0 = 544, y0 = 1216,
    x1 = 544, y1 = 1296,
    period = 5,
    t = 1.8125,
    frame = 5,
    fps = 16,
  },
  {
    type = 4,
    x0 = 3776, y0 = 1800,
    x1 = 3776, y1 = 1864,
    period = 4,
    t = 1.25,
    frame = 4,
    fps = 16,
  },
  {
    type = 4,
    x0 = 3632, y0 = 652,
    x1 = 3632, y1 = 732,
    period = 5,
    t = 1.625,
    frame = 2,
    fps = 16,
  },
  {
    type = 4,
    x0 = 2208, y0 = 1164,
    x1 = 2208, y1 = 1244,
    period = 5,
    t = 0.6875,
    frame = 3,
    fps = 16,
  },
  {
    type = 4,
    x0 = 1008, y0 = 1676,
    x1 = 1008, y1 = 1740,
    period = 4,
    t = 1.1875,
    frame = 3,
    fps = 16,
  },
  {
    type = 1,
    x0 = 1936, y0 = 1686,
    x1 = 2030, y1 = 1686,
    period = 6,
    t = 0.5625,
    frame = 6,
    fps = 16,
  },
  {
    type = 1,
    x0 = 688, y0 = 1760,
    x1 = 846, y1 = 1760,
    period = 10,
    t = 2.0625,
    frame = 6,
    fps = 16,
  },
  {
    type = 1,
    x0 = 3472, y0 = 1354,
    x1 = 3550, y1 = 1354,
    period = 5,
    t = 0,
    frame = 0,
    fps = 16,
  },
  {
    type = 1,
    x0 = 3840, y0 = 1120,
    x1 = 3934, y1 = 1120,
    period = 6,
    t = 2,
    frame = 7,
    fps = 16,
  },
  {
    type = 1,
    x0 = 3840, y0 = 774,
    x1 = 3902, y1 = 774,
    period = 4,
    t = 1.5,
    frame = 7,
    fps = 16,
  },
  {
    type = 1,
    x0 = 3200, y0 = 864,
    x1 = 3358, y1 = 864,
    period = 10,
    t = 3.4375,
    frame = 7,
    fps = 16,
  },
  {
    type = 1,
    x0 = 2944, y0 = 1188,
    x1 = 3038, y1 = 1188,
    period = 6,
    t = 0.8125,
    frame = 2,
    fps = 16,
  },
  {
    type = 1,
    x0 = 3072, y0 = 1886,
    x1 = 3230, y1 = 1886,
    period = 10,
    t = 3.0625,
    frame = 6,
    fps = 16,
  },
  {
    type = 5,
    x0 = 416, y0 = 782,
    x1 = 558, y1 = 782,
    period = 9,
    t = 3.0625,
    frame = 1,
    fps = 16,
  },
  {
    type = 5,
    x0 = 2080, y0 = 1504,
    x1 = 2238, y1 = 1504,
    period = 10,
    t = 4.75,
    frame = 4,
    fps = 16,
  },
  {
    type = 5,
    x0 = 784, y0 = 1376,
    x1 = 862, y1 = 1376,
    period = 5,
    t = 1.625,
    frame = 2,
    fps = 16,
  },
  {
    type = 5,
    x0 = 2560, y0 = 654,
    x1 = 2718, y1 = 654,
    period = 10,
    t = 1.875,
    frame = 1,
    fps = 16,
  },
  {
    type = 5,
    x0 = 3856, y0 = 1248,
    x1 = 3934, y1 = 1248,
    period = 5,
    t = 1.875,
    frame = 6,
    fps = 16,
  },
  {
    type = 5,
    x0 = 1344, y0 = 778,
    x1 = 1502, y1 = 778,
    period = 10,
    t = 2.1875,
    frame = 3,
    fps = 16,
  },
  {
    type = 5,
    x0 = 3200, y0 = 1312,
    x1 = 3294, y1 = 1312,
    period = 6,
    t = 0.0625,
    frame = 6,
    fps = 16,
  },
  {
    type = 5,
    x0 = 1984, y0 = 920,
    x1 = 2126, y1 = 920,
    period = 9,
    t = 1.875,
    frame = 1,
    fps = 16,
  },
  {
    type = 6,
    x0 = 1248, y0 = 642,
    x1 = 1310, y1 = 642,
    period = 4,
    t = 1.625,
    frame = 2,
    fps = 16,
  },
  {
    type = 6,
    x0 = 1152, y0 = 1184,
    x1 = 1246, y1 = 1184,
    period = 6,
    t = 2,
    frame = 0,
    fps = 16,
  },
  {
    type = 6,
    x0 = 176, y0 = 1358,
    x1 = 270, y1 = 1358,
    period = 6,
    t = 2.8125,
    frame = 2,
    fps = 16,
  },
  {
    type = 6,
    x0 = 2560, y0 = 1210,
    x1 = 2718, y1 = 1210,
    period = 10,
    t = 0.5625,
    frame = 6,
    fps = 16,
  },
  {
    type = 6,
    x0 = 1808, y0 = 974,
    x1 = 1886, y1 = 974,
    period = 5,
    t = 0.125,
    frame = 5,
    fps = 16,
  },
  {
    type = 6,
    x0 = 3232, y0 = 1056,
    x1 = 3342, y1 = 1056,
    period = 7,
    t = 0.1875,
    frame = 3,
    fps = 16,
  },
  {
    type = 6,
    x0 = 3840, y0 = 970,
    x1 = 3934, y1 = 970,
    period = 6,
    t = 2.125,
    frame = 2,
    fps = 16,
  },
  {
    type = 6,
    x0 = 2512, y0 = 1800,
    x1 = 2574, y1 = 1800,
    period = 4,
    t = 1.5625,
    frame = 1,
    fps = 16,
  },
  {
    type = 7,
    x0 = 1622, y0 = 974,
    x1 = 1684, y1 = 974,
    period = 4,
    t = 1.125,
    frame = 5,
    fps = 16,
  },
  {
    type = 7,
    x0 = 1638, y0 = 1310,
    x1 = 1796, y1 = 1310,
    period = 10,
    t = 4.5625,
    frame = 1,
    fps = 16,
  },
  {
    type = 7,
    x0 = 3654, y0 = 1096,
    x1 = 3716, y1 = 1096,
    period = 4,
    t = 1,
    frame = 0,
    fps = 16,
  },
  {
    type = 7,
    x0 = 2438, y0 = 824,
    x1 = 2500, y1 = 824,
    period = 4,
    t = 1.125,
    frame = 2,
    fps = 16,
  },
  {
    type = 7,
    x0 = 1414, y0 = 1706,
    x1 = 1476, y1 = 1706,
    period = 4,
    t = 0.375,
    frame = 1,
    fps = 16,
  },
  {
    type = 7,
    x0 = 278, y0 = 1722,
    x1 = 436, y1 = 1722,
    period = 10,
    t = 0.3125,
    frame = 5,
    fps = 16,
  },
  {
    type = 7,
    x0 = 3062, y0 = 704,
    x1 = 3140, y1 = 704,
    period = 5,
    t = 1,
    frame = 0,
    fps = 16,
  },
  {
    type = 7,
    x0 = 2934, y0 = 476,
    x1 = 3028, y1 = 476,
    period = 6,
    t = 2.125,
    frame = 5,
    fps = 16,
  },
}

local orbs = {
  -- Mass from CoM calculation: m2 = m1 * (c / (L - c))
  -- m2 = mass of orb, m1 = mass of ship, L = length of tractor = 40
  -- c = CoM displacement (away from ship), which is the value in the table.
  { x =  777, y =  328, m = 4                  }, -- orb 01 CoM=32
  { x =  393, y = 1048, m = 4                  }, -- orb 02 CoM=32
  { x = 2825, y =  328, m = 3.4444444444444446 }, -- orb 03 CoM=31
  { x = 3465, y = 1048, m = 3.4444444444444446 }, -- orb 04 CoM=31
  { x = 2377, y =  984, m = 3                  }, -- orb 05 CoM=30
  { x = 1433, y = 1192, m = 2.6363636363636362 }, -- orb 06 CoM=29
  { x = 3465, y = 1848, m = 2.3333333333333335 }, -- orb 07 CoM=28
  { x = 3465, y =  728, m = 2.0769230769230770 }, -- orb 08 CoM=27
  { x = 1305, y =  616, m = 1.8571428571428572 }, -- orb 09 CoM=26
  { x = 2009, y = 1512, m = 1.6666666666666667 }, -- orb 10 CoM=25
  { x =  969, y = 1368, m = 1.3529411764705883 }, -- orb 11 CoM=23
  { x =  649, y = 1496, m = 1.1052631578947370 }, -- orb 12 CoM=21
  { x = 2905, y =  680, m = 0.8181818181818182 }, -- orb 13 CoM=18
  { x =  153, y = 1192, m = 0.5384615384615384 }, -- orb 14 CoM=14
  { x = 3913, y = 1496, m = 0.2903225806451613 }, -- orb 15 CoM=9
  { x =  329, y = 1880, m = 0.2903225806451613 }, -- orb 16 CoM=9
}

local decoys = {
  { x = 3273, y = 1880 }, -- decoy 01
  { x = 3305, y = 1880 }, -- decoy 02
  { x = 3337, y = 1848 }, -- decoy 03
  { x = 3369, y = 1848 }, -- decoy 04
  { x = 3401, y = 1784 }, -- decoy 05
  { x = 3433, y = 1784 }, -- decoy 06
  { x = 3497, y = 1848 }, -- decoy 07
  { x = 3529, y = 1752 }, -- decoy 08
  { x = 3561, y = 1752 }, -- decoy 09
  { x = 3593, y = 1880 }, -- decoy 10
  { x = 3625, y = 1880 }, -- decoy 11
}

local targets = {
  { x = 58, y =  8, tile = 68, empty = 1 },
  { x = 57, y =  8, tile = 67, empty = 1 },
  { x = 58, y =  9, tile = 66, empty = 1 },
  { x = 57, y =  9, tile = 65, empty = 1 },
  { x = 58, y = 10, tile = 64, empty = 1 },
  { x = 57, y = 10, tile = 63, empty = 1 },
  { x = 58, y = 11, tile = 62, empty = 1 },
  { x = 57, y = 11, tile = 61, empty = 1 },
  { x = 59, y = 12, tile = 60, empty = 1 },
  { x = 58, y = 12, tile = 59, empty = 1 },
  { x = 57, y = 12, tile = 59, empty = 1 },
  { x = 56, y = 12, tile = 58, empty = 1 },
  { x = 59, y = 13, tile = 57, empty = 1 },
  { x = 58, y = 13, tile = 57, empty = 1 },
  { x = 57, y = 13, tile = 57, empty = 1 },
  { x = 56, y = 13, tile = 57, empty = 1 },
}

-- Where to revive in case of crash (assumed wxh from topleftx/toplefty)
-- When the ship enters one of these squares, it's registered, and the next
-- time it's respawned at the given x,y
local respawn = {
  {
    topleftx = 0,
    toplefty = 0,
    w = 384,
    h = 384,
    x = 256,
    y = 158,
  },
  {
    topleftx = 512,
    toplefty = 0,
    w = 384,
    h = 384,
    x = 768,
    y = 170,
  },
  {
    topleftx = 1024,
    toplefty = 0,
    w = 384,
    h = 384,
    x = 1374,
    y = 146,
  },
  {
    topleftx = 1536,
    toplefty = 0,
    w = 384,
    h = 384,
    x = 1790,
    y = 146,
  },
  {
    topleftx = 2048,
    toplefty = 0,
    w = 384,
    h = 384,
    x = 2302,
    y = 146,
  },
  {
    topleftx = 2560,
    toplefty = 0,
    w = 384,
    h = 384,
    x = 2814,
    y = 146,
  },
  {
    topleftx = 3072,
    toplefty = 0,
    w = 384,
    h = 384,
    x = 3286,
    y = 146,
  },
  {
    topleftx = 3584,
    toplefty = 0,
    w = 384,
    h = 384,
    x = 3838,
    y = 146,
  },
  {
    topleftx = 320,
    toplefty = 768,
    w = 384,
    h = 384,
    x = 544,
    y = 898,
  },
  {
    topleftx = 576,
    toplefty = 1152,
    w = 384,
    h = 384,
    x = 822,
    y = 1266,
  },
  {
    topleftx = 1152,
    toplefty = 640,
    w = 384,
    h = 384,
    x = 1310,
    y = 770,
  },
  {
    topleftx = 128,
    toplefty = 1408,
    w = 384,
    h = 384,
    x = 358,
    y = 1522,
  },
  {
    topleftx = 512,
    toplefty = 1536,
    w = 384,
    h = 384,
    x = 894,
    y = 1690,
  },
  {
    topleftx = 896,
    toplefty = 1536,
    w = 384,
    h = 384,
    x = 1134,
    y = 1658,
  },
  {
    topleftx = 1472,
    toplefty = 1152,
    w = 384,
    h = 384,
    x = 1598,
    y = 1274,
  },
  {
    topleftx = 1920,
    toplefty = 896,
    w = 384,
    h = 384,
    x = 2086,
    y = 1034,
  },
  {
    topleftx = 1856,
    toplefty = 1472,
    w = 384,
    h = 384,
    x = 2070,
    y = 1586,
  },
  {
    topleftx = 2304,
    toplefty = 1408,
    w = 384,
    h = 384,
    x = 2542,
    y = 1562,
  },
  {
    topleftx = 2752,
    toplefty = 1600,
    w = 384,
    h = 384,
    x = 3006,
    y = 1722,
  },
  {
    topleftx = 3200,
    toplefty = 1600,
    w = 384,
    h = 384,
    x = 3478,
    y = 1722,
  },
  {
    topleftx = 2048,
    toplefty = 512,
    w = 384,
    h = 384,
    x = 2366,
    y = 650,
  },
  {
    topleftx = 2560,
    toplefty = 768,
    w = 384,
    h = 384,
    x = 2758,
    y = 906,
  },
  {
    topleftx = 2816,
    toplefty = 1152,
    w = 384,
    h = 384,
    x = 3014,
    y = 1290,
  },
  {
    topleftx = 3200,
    toplefty = 1152,
    w = 384,
    h = 384,
    x = 3358,
    y = 1290,
  },
  {
    topleftx = 3776,
    toplefty = 704,
    w = 384,
    h = 384,
    x = 3918,
    y = 850,
  },
}

-- tile -> enemy type (for agents)
local agents = {
  [111] = 1, [117] = 2, [116] = 3, [118] = 4,
  [119] = 5, [120] = 6, [121] = 7, [115] = 8,
}

-- require() can't return multiple values, so we wrap them in a function
return function()
  return map, enemytypes, enemies, orbs, decoys, targets, respawn, agents
end
