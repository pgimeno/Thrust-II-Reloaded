# THRUST II RELOADED

This game is a remake of the classic Thrust II game, ZX Spectrum version.

For copyright reasons, one directory is incomplete, img/ which should contain the game graphics. As new graphics are created, the img/ directory will be populated with them.

There's a complete version and full instructions in this thread:
https://www.love2d.org/forums/viewtopic.php?f=5&t=81198

## Authors

See [AUTHORS.txt](AUTHORS.txt)
