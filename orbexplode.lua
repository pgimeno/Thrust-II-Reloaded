--[========================================================================[--

Orb Explosion game over screen for Thrust II Reloaded.

Copyright © 2015-2018 Pedro Gimeno Fortea

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

--]========================================================================]--

local orbexplode = {}

local la,le,lfs,lf,lg,li,lj,lk,lm,lmo,lp,ls,lsys,lth,lt,lw = require'ns'()

local time

function orbexplode.activate()
  time = 0
end

function orbexplode.update(dt)
  time = time + dt
  if time >= 5 then
    main.activate(screens.menu)
    return
  end
end

function orbexplode.draw()
  main.centertext("THE ORB EXPLODED AND", main.wcx, main.wcy-16)
  main.centertext("DESTROYED THE PLANET", main.wcx, main.wcy)
  main.centertext("[THIS SCREEN IS WORK IN PROGRESS, SORRY]", main.wcx, main.wcy+16)
end

function orbexplode.keypressed(k, r)
  if r then return end
  if k == "escape" then
    return main.dialog("EXIT TO MENU?", main.tomenu)
  end
end

return orbexplode
