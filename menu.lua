--[========================================================================[--

Menu screen code for Thrust II Reloaded.

Copyright © 2015-2018 Pedro Gimeno Fortea

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

--]========================================================================]--

local menu = {}

local la,le,lfs,lf,lg,li,lj,lk,lm,lmo,lp,ls,lsys,lth,lt,lw = require'ns'()

local menuimage
local has_save
local t
local playing
local period = 1.9961678004535148 -- 88031 samples / 44100 Hz
local bar1, bar2
local barc
local snd
local activeMenu
local sndstate = "stopped"

local DIV = love_version >= 11000000 and 255 or 1

function menu.load()
  menuimage = lg.newImage("img/mainmenu.png")
--    playground~/images/ThrustII-newlogo-isolated.png")
  snd = la.newSource("snd/Thrust II - menu.ogg", "stream")
  snd:setVolume(0.5)
  snd:setLooping(true)
end

function menu.activate()
  has_save = main.isFile("saved.txt")
  playing = false
  activeMenu = "main"
end

function menu.deactivate()
  snd:stop()
  sndstate = "stopped"
end

function menu.pause(paused)
  if playing then
    if paused then
      if sndstate == "playing" then
        snd:pause()
        sndstate = "paused"
      end
    elseif sndstate == "paused" then
      snd:play()
      sndstate = "playing"
    end
  end
end

function menu.update(dt)
  -- This is obsolete and was used for synchronization of the audio effect with the bars.
  if not playing then
    -- start playing (not done earlier because we don't know how long it would take)
    playing = true
    t = period/2
    if main.music then
      snd:play()
      sndstate = "playing"
    end
    bar1 = main.wcy
    bar2 = bar1
    barc = 1
  else
    t = t + dt
    if t >= period then
      t = t - period
    end
    bar1 = main.wcy - (t-period/2)/period * main.wh*0.78
    bar2 = main.wcy + (t-period/2)/period * main.wh*0.78
    barc = barc + dt * 50
  end
end

function menu.draw()
  if playing then
    local c = math.floor(barc) % 6 + 1
    local b = c % 2  c = (c - b) / 2
    local r = c % 2  c = (c - r) / 2
    local g = c % 2
    lg.setColor(r*(192/DIV), g*(192/DIV), b*(192/DIV))
    lg.rectangle("fill", 0, bar1-2, main.ww, 5)
    lg.rectangle("fill", 0, bar2-2, main.ww, 5)
  end
  lg.setColor(255/DIV,255/DIV,255/DIV)
  main.drawaligned(5, menuimage, main.wcx, main.wcy)
--  screens.game.tiles_draw(2176, 32)
--  do return end

  lg.setColor(0,192/DIV,0)
  main.centertext("@^ A.ROGERS 1986", main.wcx, main.wcy)
  main.centertext("@^ P.GIMENO 2017", main.wcx, main.wcy+24)
  lg.setColor(192/DIV,192/DIV,192/DIV)
  if activeMenu == "main" then
    main.centertext("1....START  GAME", main.wcx, main.wcy+32*3)
    main.centertext("2....DEFINE KEYS", main.wcx, main.wcy+32*4)
    main.centertext("3....SET OPTIONS", main.wcx, main.wcy+32*5)
    if has_save then
      main.centertext("4....LOAD   GAME", main.wcx, main.wcy+32*6)
    end
  elseif activeMenu == "options" then
    local musOnOff
    if main.music then
      musOnOff = "-  ON"
    else
      musOnOff = "- OFF"
    end
    main.centertext(string.format("1....ZOOM - %3.2gX", main.pixelsize), main.wcx, main.wcy+32*3)
    main.centertext(main.fullscreen and "2....WINDOW MODE" or "2....FULL SCREEN", main.wcx, main.wcy+32*4)
    main.centertext("3....MUSIC "..musOnOff, main.wcx, main.wcy+32*5)
    main.centertext("0....MAIN   MENU", main.wcx, main.wcy+32*7)
  end
  lg.setColor(255/DIV,255/DIV,255/DIV)
end

function menu.keypressed(k)
  if k == "escape" then
    if activeMenu == "options" then
      activeMenu = "main"
    else
      le.quit()
    end
  end
end

function menu.textinput(c)
  if activeMenu == "main" then
    if c == "1" then
      main.restore = false
      screens.game.newgame()
      screens.getready.fromstart = true
      main.activate(screens.getready)
      return
    elseif c == "2" then
      main.activate(screens.redef)
      return
    elseif c == "3" then
      activeMenu = "options"
      return
    elseif c == "4" and has_save then
      main.restore = true
      screens.game.newgame()
      screens.getready.fromstart = true
      return main.activate(screens.getready)
    end
  elseif activeMenu == "options" then
    if c == "0" then
      activeMenu = "main"
    elseif c == "1" then
      main.pixelsize = main.pixelsize + 0.5
      if main.pixelsize > 4 then
        main.pixelsize = 1
      end
      love.resize(lg.getDimensions())
      main.saveScreen()
    elseif c == "2" then
      main.fullscreen = not main.fullscreen
      main.setMode()
      main.saveScreen()
      -- HACK: Apparently LÖVE neglects to call love.resize after entering/exiting fullscreen.
      love.resize(lg.getDimensions())
    elseif c == "3" then
      main.music = not main.music
      if main.music then
        snd:play()
        sndstate = "playing"
      else
        snd:stop()
        sndstate = "stopped"
      end
      main.setVolume(main.vol, true) -- save music setting
    end
  end
end

return menu
